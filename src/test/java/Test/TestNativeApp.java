package Test;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by AmitW on 7/10/2017.
 */
public class TestNativeApp {

    // create global variable
    private static AndroidDriver driver;

    public static void main(String [] args) throws MalformedURLException, InterruptedException {

        // Create object of DesiredCapabilities class
        DesiredCapabilities capabilities = new DesiredCapabilities();

        // Optional
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "");

        // Specify the device name (any name)
        capabilities.setCapability("deviceName", "ZY22252F5B");

        // Platform version
        capabilities.setCapability("platformVersion", "6.0");

        // platform name
        capabilities.setCapability("platformName", "Android");

        // specify the application package that we copied from appium
        capabilities.setCapability("appPackage", "io.selendroid.testapp");

        // specify the application activity that we copied from appium
        capabilities.setCapability("appActivity", ".HomeScreenActivity");

        // Start android driver
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

        // Specify the implicit wait of 5 second
       // driver.manage().timeouts().implicitlyWait(5, TimUnit.SECONDS);

        //Enter the text in textbox
        driver.findElement(By.className("android.widget.EditText")).sendKeys("Amit");

        // click on registration button
        driver.findElement(By.id("io.selendroid.testapp:id/startUserRegistration")).click();

        // Wait for 10 second
        Thread.sleep(10000);

        // close the application
        driver.quit();



    }
}
