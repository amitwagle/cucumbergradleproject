package Test;

import Pages.DriverFactory;
import Pages.ReadPropertyFile;
import com.sun.org.apache.xerces.internal.impl.PropertyManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.IOException;

/**
 * Created by AmitW on 6/17/2017.
 */
public class TestSample {
    //public WebDriver driver;

    public static void main(String[] args) throws InterruptedException, IOException {

        ReadPropertyFile reader = new ReadPropertyFile();
        WebDriver driver = DriverFactory.getDriver(reader.getBrowser());

      // System.setProperty("webdriver.chrome.driver","./src/test/resources/drivers/chromedriver.exe");
       //ChromeDriver driver = new ChromeDriver();

        //System.setProperty("webdriver.ie.driver","D:\\Selenium Jar Files\\Browser drivers\\IEDriverServer.exe");
        //InternetExplorerDriver driver = new InternetExplorerDriver();

        //System.setProperty("webdriver.gecko.driver","D:\\Selenium Jar Files\\Browser drivers\\geckodriver.exe");
        //FirefoxDriver driver = new FirefoxDriver();


        driver.get("http://newtours.demoaut.com/");
        Thread.sleep(4000);

        boolean blnLoginPage = driver.findElement(By.linkText("SIGN-ON")).isDisplayed();
        System.out.println(blnLoginPage);

        driver.findElement(By.name("userName")).sendKeys("mercury");
        driver.findElement(By.name("password")).sendKeys("mercury");
        driver.findElement(By.name("login")).click();
        Thread.sleep(4000);

        boolean blnHomePage = driver.findElement(By.linkText("SIGN-OFF")).isDisplayed();
        System.out.println(blnHomePage);
        driver.close();
    }
}
