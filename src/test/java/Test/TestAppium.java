package Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by AmitW on 7/3/2017.
 */
public class TestAppium {

    WebDriver driver;

    @Test
   public void Test01() throws MalformedURLException {

    // Created object of DesiredCapabilities class.
    DesiredCapabilities capabilities = new DesiredCapabilities();

    // Set android deviceName desired capability. Set it Android Emulator.
    capabilities.setCapability("deviceName", "Android Emulator");

    // Set browserName desired capability. It's Android in our case here.
    capabilities.setCapability("browserName", "Android");

    // Set android platformVersion desired capability. Set your emulator's android version.
    capabilities.setCapability("platformVersion", "6.0");
       // capabilities.setCapability(CapabilityType.VERSION, "6.0");

    // Set android platformName desired capability. It's Android in our case here.
    capabilities.setCapability("platformName", "Android");

    // Set android appPackage desired capability. It is com.android.calculator2 for calculator application.
    capabilities.setCapability("appPackage", "com.android.calculator2");

    // Set android appActivity desired capability. It is com.android.calculator2.Calculator for calculator application.
    capabilities.setCapability("appActivity", "com.android.calculator2.Calculator");

    // Created object of RemoteWebDriver will all set capabilities.
    driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
    driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

    // Click on CLR button.
    //driver.findElement(By.name("del")).click();

    // Click on number 2 button.
    driver.findElement(By.name("2")).click();
    driver.findElement(By.name("+")).click();

    // Click on number 3 button.
    driver.findElement(By.name("3")).click();




   }

}
