package Test;

import java.sql.*;

/**
 * Created by AmitW on 6/24/2017.
 */
public class TestDB {

    public static void main(String[] args) throws SQLException {
    //Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    Connection conn = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=Hospital;IntegratedSecurity=true");

    Statement st = conn.createStatement();
    String Sql = "Select * from Doctor";
    ResultSet rs = st.executeQuery(Sql);
        while (rs.next()) {
        String DoctorName=rs.getString("DoctorName");
        String DoctorSpecialization=rs.getString("DoctorSpecialization");
        System.out.println(DoctorName+"\t\t"+DoctorSpecialization);
    }
    rs.close();
   }
}
