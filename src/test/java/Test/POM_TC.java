package Test;

import Pages.LogIn_Page;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by AmitW on 6/18/2017.
 */
public class POM_TC {

    public static void main(String[] args){

        System.setProperty("webdriver.chrome.driver", "./src/test/resources/drivers/chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();

        //driver.manage().window().maximize();
        driver.get("http://newtours.demoaut.com/");

        //Use page Object library now
        LogIn_Page ObjLogin = new LogIn_Page(driver);
        ObjLogin.LoginToMercuryTours("mercury", "mercury");

    }
}
