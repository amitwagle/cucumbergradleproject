package Test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by AmitW on 6/18/2017.
 */
public class TestRadioButton_ListBox {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","./src/test/resources/drivers/chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();

        //driver.manage().window().maximize();
        driver.get("http://newtours.demoaut.com/");
        Thread.sleep(4000);

        //Enter Username and password
        driver.findElement(By.name("userName")).sendKeys("mercury");
        driver.findElement(By.name("password")).sendKeys("mercury");
        driver.findElement(By.name("login")).click();
        Thread.sleep(4000);

        //Select Flight
        driver.findElement(By.linkText("Flights")).click();

        //Select Type (One way radio button)
        List<WebElement> oRadioButton = driver.findElements(By.name("tripType"));
        int iSize = oRadioButton.size();
        System.out.println(iSize);

        for (int i = 0; i < iSize; i++) {

            String sValue = oRadioButton.get(i).getAttribute("value");
            if (sValue.equalsIgnoreCase("oneway")) {
                oRadioButton.get(i).click();
                break;
            }

        }

        //Select Passengers
        Select passCount = new Select(driver.findElement(By.name("passCount")));
        passCount.selectByVisibleText("2");

        //Select Departing From
        Select fromPort = new Select(driver.findElement(By.name("fromPort")));
        fromPort.selectByVisibleText("London");

        //Select From Month
        Select fromMonth = new Select(driver.findElement(By.name("fromMonth")));
        fromMonth.selectByVisibleText("July");

        //Select From Day
        Select fromDay = new Select(driver.findElement(By.name("fromDay")));
        fromDay.selectByVisibleText("20");

        //Select Arriving In
        Select toPort = new Select(driver.findElement(By.name("toPort")));
        fromPort.selectByVisibleText("Paris");

        //Select To Month
        Select toMonth = new Select(driver.findElement(By.name("toMonth")));
        toMonth.selectByVisibleText("August");

        //Select To Day
        Select toDay = new Select(driver.findElement(By.name("toDay")));
        toDay.selectByVisibleText("20");

        //Select Service Class
        List<WebElement> oServiceClass = driver.findElements(By.name("servClass"));
        int iServiceClassSize = oServiceClass.size();
        System.out.println(iServiceClassSize);

        for (int i = 0; i < iServiceClassSize; i++) {

            String sValue = oServiceClass.get(i).getAttribute("value");
            if (sValue.equalsIgnoreCase("Business")) {
                oServiceClass.get(i).click();
                break;
            }

        }

        //Select Airline
        Select airline = new Select(driver.findElement(By.name("airline")));
        airline.selectByVisibleText("Unified Airlines");

        //Click on Continue
        driver.findElement(By.name("findFlights")).click();

        Thread.sleep(4000);
        driver.close();

    }
}