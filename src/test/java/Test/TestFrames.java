package Test;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by AmitW on 6/18/2017.
 */
public class TestFrames {
    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver","./src/test/resources/drivers/chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();

        driver.get("http://demo.guru99.com/selenium/guru99home/");
        Thread.sleep(4000);
        driver.switchTo().frame("a077aa5e");
        driver.findElement(By.xpath("html/body/a/img")).click();
        driver.switchTo().parentFrame();
        Thread.sleep(4000);

        driver.quit();
    }
}
