package Test;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by AmitW on 6/18/2017.
 */
public class TestWindows {
    public static void main(String[] args){
        System.setProperty("webdriver.chrome.driver", "./src/test/resources/drivers/chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();

        //driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/popup.php");

        String MainWindow = driver.getWindowHandle();
        System.out.println(MainWindow);

        driver.findElement(By.linkText("Click Here")).click();

        //To handle all new open window
        Set<String> s1 = driver.getWindowHandles();
        System.out.println(s1);

        Iterator<String> i1 = s1.iterator();
        System.out.println(i1);


        while(i1.hasNext())
        {
            String ChildWindow = i1.next();
            System.out.println(ChildWindow);

            if(!MainWindow.equalsIgnoreCase(ChildWindow)) {

                driver.switchTo().window(ChildWindow);
                driver.findElement(By.name("emailid")).sendKeys("gavrav.3n@gmail.com");
                driver.findElement(By.name("btnLogin")).click();
                driver.close();

            }
        }
        driver.switchTo().window(MainWindow);
        driver.close();
    }
}
