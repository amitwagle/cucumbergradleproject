package Test;


import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;

/**
 * Created by AmitW on 6/27/2017.
 */
public class WeatherGetRequests {

    //simple get request for getting weather request by City name
    //Status code : 200
    @Test
    public void Test01(){
        Response resp = when().
                get("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b1b15e88fa797225412429c1c50c122a1").
                then().
                contentType(ContentType.JSON).extract().response();
        System.out.println(resp.getStatusCode());
        Assert.assertEquals(resp.getStatusCode(), 200);
        System.out.println(resp.asString());

    }

    //Status code : 401
    @Test
    public void Test02(){
        Response resp = when().get("http://samples.openweathermap.org/data/2.5/weather?q=London,appid=b1b15e88fa797225412429c1c50c122a");
        System.out.println(resp.getStatusCode());
        Assert.assertEquals(resp.getStatusCode(), 401);

    }

    //parameterising
    @Test
    public void Test03(){
        Response resp = given().
                param("q", "London").
                param("appid", "b1b15e88fa797225412429c1c50c122a1").
                when().get("http://samples.openweathermap.org/data/2.5/weather");
        System.out.println(resp.getStatusCode());
        Assert.assertEquals(resp.getStatusCode(), 200);

        if (resp.getStatusCode()==200){
            System.out.println("API is working fine");
        }
        else{
            System.out.println("API is not working fine");
        }

    }

    //using assertThat
    @Test
    public void Test04() {
        given().
        param("q", "London").
        param("appid", "b1b15e88fa797225412429c1c50c122ae").
        when().
        get("http://samples.openweathermap.org/data/2.5/weather").
        then().
        assertThat().statusCode(200);
    }


    //retriving response i.e json file content
    @Test
    public void Test05() {
        Response resp = given().
        param("q", "London,uk").
        param("appid", "b1b15e88fa797225412429c1c50c122a1").
        when().
        get("http://samples.openweathermap.org/data/2.5/weather");

        System.out.println(resp.asString());
    }

    //extracting the content
    @Test
    public void Test06() {
        String weatherReport = given().
                param("q", "London,uk").
                param("appid", "b1b15e88fa797225412429c1c50c122a1").
                when().
                get("http://samples.openweathermap.org/data/2.5/weather").
                then().
                contentType(ContentType.JSON).
                extract().
                path("weather[0].description");


        System.out.println("weather report : " + weatherReport);
    }

    //extracting the content and verifying in proper way
    @Test
    public void Test07() {
        Response resp = given().
                param("q", "London").
                param("appid", "b1b15e88fa797225412429c1c50c122a1").
                when().
                get("http://samples.openweathermap.org/data/2.5/weather");

        String actualWeatherReport = resp.
                then().
                contentType(ContentType.JSON).
                extract().
                path("weather[0].description");

        String expectedWeatherreport = "light intensity drizzle";

        if(actualWeatherReport.equalsIgnoreCase(expectedWeatherreport)){
            System.out.println("Testcase pass");
        }
        else{
            System.out.println("Testcase fail");
        }

    }

}
