package Test;


import Pages.ServiceTest;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.junit.Test;


import static com.jayway.restassured.RestAssured.given;


/**
 * Created by AmitW on 6/28/2017.
 */
public class JasonServerRequests {

    //Get Status Code
    @Test
    public void Test01(){
        Response resp = given().
                        when().
                        get("http://localhost:3000/posts");

        System.out.println("Get response status : " + resp.getStatusCode());
        System.out.println("Get response : " + resp.asString());

    }

    //Post a request
    @Test
    public void Test02(){
        Response resp = given().
                        body(" {\"id\":\"4\",\"title\":\"Title4\",\"author\":\"Bhuvan\"} ").
                        when().
                        contentType(ContentType.JSON).
                        post("http://localhost:3000/posts");

        System.out.println("Get response is : " + resp.getStatusCode());
    }


    //get the particular value from json
    @Test
    public void Test03(){

        Response resp = given().
                        when().
                        get("http://localhost:3000/posts").
                        then().
                        contentType(ContentType.JSON).
                        extract().
                        path("books[0].id");

        System.out.println("response" + resp.getStatusCode());
        System.out.println("response" + resp);
    }

    //Post a request by object
    @Test
    public void Test04(){

        ServiceTest serviceTest = new ServiceTest();
        serviceTest.setId("2");
        serviceTest.setTitle("title2");
        serviceTest.setAuthor("Author2");

        Response resp = given().
                        when().
                        contentType(ContentType.JSON).
                        body(serviceTest).
                        post("http://localhost:3000/posts");

        System.out.println("response" + resp.getStatusCode());

    }

    //Update a request
    @Test
    public void Test05(){
        ServiceTest serviceTest = new ServiceTest();
        serviceTest.setId("4");
        serviceTest.setTitle("Updated title4");
        serviceTest.setAuthor("Updated Author4");

        Response resp = given().
                body(serviceTest).
                when().
                contentType(ContentType.JSON).
                put("http://localhost:3000/serviceTest/5");

        System.out.println("Put API response" + resp.getStatusCode());
    }

    //update a request by patch
    @Test
    public void Test06(){
        Response resp = given().
                        body("{\"title\":\"updated by Patch request\"}").
                        when().
                        contentType(ContentType.JSON).
                        patch("http://localhost:3000/posts/3");

        System.out.println("Patch request" + resp.getStatusCode());

    }

     //Delete a request
    @Test
    public void Test07(){

        Response resp = given().
                when().
                delete("http://localhost:3000/posts/2");

        System.out.println("Deleting response" + resp.getStatusCode());
    }

}
