package Test;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
/**
 * Created by AmitW on 6/24/2017.
 */
public class TestRestService {

    public static void main(String[] args){

        given().
            get("http://parabank.parasoft.com/parabank/services/bank/customers/12212/").
        then().
            assertThat().body("customer.id", equalTo("12212")).
        and().
            assertThat().body("customer.firstName", equalTo("John")).
        and().
            assertThat().body("customer.lastName", equalTo("Smith"));


    }
}
