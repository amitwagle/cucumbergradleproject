package Test;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by AmitW on 7/6/2017.
 */
public class TestAmazon {

    WebDriver driver;
    WebDriverWait wait;

    @Test
    public void setup() throws MalformedURLException {

        // Create an object for Desired Capabilities
        DesiredCapabilities capabilities = new DesiredCapabilities();

        // Name of mobile web browser
        capabilities.setCapability("browserName", "");

        // The kind of mobile device
        capabilities.setCapability("deviceName", "ZY22252F5B");

        // Which mobile OS platform to use
        capabilities.setCapability("platformName", "Android");

        // Java package of the Android app you want to run-
        capabilities.setCapability("appPackage", "in.amazon.mShop.android.shopping");

        // Activity name for the Android activity you want to launch from your
        capabilities.setCapability("appActivity", "com.amazon.mShop.home.HomeActivity");

        // Initialize the driver object with the URL to Appium Server and
        // passing the capabilities
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        wait = new WebDriverWait(driver, 5);



    }
}