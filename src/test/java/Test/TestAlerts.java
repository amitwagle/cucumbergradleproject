package Test;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by bhuvanp on 6/15/2017.
 */
public class TestAlerts {
    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver","./src/test/resources/drivers/chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();



        //driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/V4/");

        driver.findElement(By.name("uid")).sendKeys("mngr79399");
        driver.findElement(By.name("password")).sendKeys("UzAqYzA");
        Thread.sleep(4000);
        driver.findElement(By.name("btnLogin")).click();

        driver.findElement(By.linkText("Delete Customer")).click();
        driver.findElement(By.name("cusid")).sendKeys("53920");
        driver.findElement(By.name("AccSubmit")).click();

        //Switching to Alert
        driver.switchTo().alert();

        //Capturing text message
        String AlertMessage = driver.switchTo().alert().getText();

        //Displaying alert message
        System.out.println(AlertMessage);

        //Accepting alert
        driver.switchTo().alert().accept();
        Thread.sleep(4000);
        driver.switchTo().alert().accept();

        //driver.findElement(By.partialLinkText("New Cust")).click();

        driver.close();

    }
}
