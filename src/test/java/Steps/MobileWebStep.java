package Steps;

import Pages.DesiredCapabilities_Page;
import Pages.ReadPropertyFile;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;

/**
 * Created by AmitW on 7/11/2017.
 */
public class MobileWebStep {
    @Given("^that the desired capabilities are set for mobile web$")
    public void thatTheDesireCapabilitiesAreSetForMobileWeb() throws Throwable {

        ReadPropertyFile  reader = new ReadPropertyFile();
        String browserName = reader.getBrowserName();
        String deviceName = reader.getdeviceName();
        String platformName = reader.getplatformName();
        String appPackage = reader.getappPackage();
        String appActivity = reader.getappActivity();

        DesiredCapabilities_Page cap = new DesiredCapabilities_Page(browserName,deviceName,platformName,appPackage,appActivity);
        cap.setDesiredCapabilities(cap);

    }
}
