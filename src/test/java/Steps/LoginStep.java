package Steps;

import Pages.LogIn_Page;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by AmitW on 6/19/2017.
 */
public class LoginStep {

    WebDriver driver = DriverBuilder.INSTANCE.getDriver();
    LogIn_Page ObjLogin = new LogIn_Page(driver);

    @Given("^I navigate to \"([^\"]*)\"$")
    public void iNavigate(String url) throws Throwable {
        driver.get(url);
       // driver.manage().window().maximize();
    }

    @And("^I enter the \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iEnterTheAnd(String arg0, String arg1) throws Throwable {
        ObjLogin.setUserName(arg0);
        ObjLogin.setPassWord(arg1);
    }

    @And("^I click on login button$")
    public void i_click_on_login_button() throws Throwable {
        //driver.findElement(By.name("login")).click();
        ObjLogin.clickLogin();
        Thread.sleep(4000);
    }

    @Then("^I should see the UserDetails page$")
    public void I_should_see_the_userform_page() throws Throwable {
        boolean blnHomePage = driver.findElement(By.linkText("SIGN-OFF")).isDisplayed();
        //ObjLogin.VerifyPage();
        System.out.println(blnHomePage);
        driver.close();
    }



}
