package Steps;

import Pages.Register_Page;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by AmitW on 6/20/2017.
 */
public class RegisterStep {

    WebDriver driver = DriverBuilder.INSTANCE.getDriver();
    Register_Page ObjRegister = new Register_Page(driver);

    @When("^I click on Register$")
    public void iClickOnRegister() throws Throwable {
        ObjRegister.clickonRegister();
    }

    @And("^I enter Contact Information \"([^\"]*)\" , \"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\"$")
    public void iEnter(String firstName, String lastName, String phone, String userName) throws Throwable {
        ObjRegister.enterContactInformation(firstName,lastName,phone,userName);
    }

    @And("^I enter Mailing Information \"([^\"]*)\" , \"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\"$")
    public void iEnterMailingInformation(String address, String city, String state, String postalCode, String country) throws Throwable {
        ObjRegister.enterMailingInformation(address, city, state, postalCode, country);
    }

    @And("^I enter User Information \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\"$")
    public void iEnterUserInformation(String username, String password, String confirmPassword) throws Throwable {
        ObjRegister.enterUserInformation(username,password,confirmPassword);
    }

    @And("^I click on submit$")
    public void iClickOnSubmit() throws Throwable {
        ObjRegister.clickSubmit();
    }

    @Then("^A new user should register$")
    public void aNewUserShouldRegister() throws Throwable {
        Thread.sleep(4000);
        driver.close();
    }

}
