package Steps;

import Pages.FlightFinder_Page;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by AmitW on 6/20/2017.
 */

public class FlightFinderStep {

    WebDriver driver = DriverBuilder.INSTANCE.getDriver();
    FlightFinder_Page ObjFlightFinder = new FlightFinder_Page(driver);

//    @Given("^I enter Flight Details$")
//    public void iEnterFlightDetails() throws Throwable {
//        //Select Type (One way radio button)
//        List<WebElement> oRadioButton = driver.findElements(By.name("tripType"));
//        int iSize = oRadioButton.size();
//        System.out.println(iSize);
//
//        for (int i = 0; i < iSize; i++) {
//
//            String sValue = oRadioButton.get(i).getAttribute("value");
//            if (sValue.equalsIgnoreCase("oneway")) {
//                oRadioButton.get(i).click();
//                break;
//            }
//
//        }
//
//        //Select Passengers
//        Select passCount = new Select(driver.findElement(By.name("passCount")));
//        passCount.selectByVisibleText("2");
//
//        //Select Departing From
//        Select fromPort = new Select(driver.findElement(By.name("fromPort")));
//        fromPort.selectByVisibleText("London");
//
//        //Select From Month
//        Select fromMonth = new Select(driver.findElement(By.name("fromMonth")));
//        fromMonth.selectByVisibleText("July");
//
//        //Select From Day
//        Select fromDay = new Select(driver.findElement(By.name("fromDay")));
//        fromDay.selectByVisibleText("20");
//
//        //Select Arriving In
//        Select toPort = new Select(driver.findElement(By.name("toPort")));
//        fromPort.selectByVisibleText("Paris");
//
//        //Select To Month
//        Select toMonth = new Select(driver.findElement(By.name("toMonth")));
//        toMonth.selectByVisibleText("August");
//
//        //Select To Day
//        Select toDay = new Select(driver.findElement(By.name("toDay")));
//        toDay.selectByVisibleText("20");
//    }

    @And("^I enter Flight Details \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\"$")
    public void iEnterFlightDetails(String strType, String strPassenger, String strDeptFrom, String strDeptMonth, String strDeptDate, String strArrivingIn, String strArrivingMonth, String strArrivingDate) throws Throwable {
        ObjFlightFinder.enterFlightDetails(strType,strPassenger,strDeptFrom,strDeptMonth,strDeptDate,strArrivingIn,strArrivingMonth,strArrivingDate);
    }

    @And("^I enter Preferences \"([^\"]*)\", \"([^\"]*)\"$")
    public void iEnterPreferences(String strServiceClass, String strAirline) throws Throwable {
        ObjFlightFinder.enterPreferences(strServiceClass,strAirline);
    }

    @Then("^I click on continue button$")
    public void iClickOnContinueButton() throws Throwable {
        ObjFlightFinder.clickContinue();
        Thread.sleep(4000);
        driver.close();
    }
}
