package Steps;
import java.sql.* ;

import Pages.Database_Page;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by AmitW on 6/27/2017.
 */

public class DatabaseStep {
    Database_Page DB;
    String sqlQuery;

    @Given("^I connect to Local SQLServer with database \"([^\"]*)\"$")
    public void iConnectToLocalSQLServerWithDatabase (String DatabaseName) throws Throwable {
       this.DB = new Database_Page("localhost","", "", DatabaseName);

    }

    @When("^I provide Doctor name \"([^\"]*)\"$")
    public void iProvideDoctorName(String strDoctorName) throws Throwable {
        this.sqlQuery = "Select * from Doctor where DoctorName = '" + strDoctorName + "'";

    }

    @Then("^Doctor Specialization should be \"([^\"]*)\"$")
    public void doctorSpecializationShouldBe(String strExpected) throws Throwable {
        String strActual = DB.QueryExecutor(sqlQuery,"DoctorSpecialization");
        Assert.assertEquals(strExpected,strActual);
    }

    @And("^Phone number should be \"([^\"]*)\"$")
    public void phoneNumberShouldBe(String strExpected) throws Throwable {
        String strActual = DB.QueryExecutor(sqlQuery,"DoctorPhoneNumber");
        Assert.assertEquals(strExpected,strActual);
    }

    @When("^I insert doctor details '(\\d+)','asdf','MD','(\\d+)'$")
    public void iInsertDoctorDetailsAsdfMD(int arg0, int arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the doctor should be added into the database$")
    public void theDoctorShouldBeAddedIntoTheDatabase() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}

