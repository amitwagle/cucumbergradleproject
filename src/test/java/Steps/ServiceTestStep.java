package Steps;

import Pages.ServiceTest;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import com.jayway.restassured.specification.RequestSpecification;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;

/**
 * Created by AmitW on 6/28/2017.
 */
public class ServiceTestStep {
    ServiceTest serviceTest = new ServiceTest();
    private Integer statusCode;

    @Given("^the api are up and running for \"([^\"]*)\"$")
    public void theApiAreUpAndRunningFor(String url) throws Throwable {
        //Assert.assertEquals(serviceTest.getServiceCode(url),200);
    }

    @When("^a user perform a get request to \"([^\"]*)\"$")
    public void aUserPerformAGetRequestTo(String url) throws Throwable {
        statusCode = serviceTest.getServiceCode(url);
    }


    @Then("^the response status code should be \"([^\"]*)\"$")
    public void theResponseStatusCodeShouldBe(Integer expStatusCode) throws Throwable {
        //System.out.println(statusCode);
        //System.out.println(expStatusCode);
        Assert.assertEquals(statusCode,expStatusCode);
    }

    @Then("^the ID should be \"([^\"]*)\"$")
    public void theIDShouldBe(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^the title should be \"([^\"]*)\"$")
    public void theTitleShouldBe(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^the author should be \"([^\"]*)\"$")
    public void theAuthorShouldBe(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }


    @When("^a user perform a post request by adding ID \"([^\"]*)\" title \"([^\"]*)\" Author \"([^\"]*)\"$")
    public void aUserPerformAPostRequestByAddingIDTitleAuthor(String id, String title, String author) throws Throwable {
        ServiceTest serviceTest = new ServiceTest();
        serviceTest.setId(id);
        serviceTest.setTitle(title);
        serviceTest.setAuthor(author);

        Response resp = given().
                when().
                contentType(ContentType.JSON).
                body(serviceTest).
                post("http://localhost:3000/books");

        statusCode = resp.getStatusCode();
        System.out.println("post response : " + statusCode);
    }

    @When("^a user perform a delete request \"([^\"]*)\"$")
    public void aUserPerformADeleteRequest(String arg0) throws Throwable {
        Response resp = given().
                when().
                delete(arg0);

        System.out.println("Deleting response" + resp.getStatusCode());
    }
}
