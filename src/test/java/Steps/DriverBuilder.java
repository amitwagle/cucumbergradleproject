package Steps;

import Pages.DriverFactory;
import Pages.ReadPropertyFile;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by AmitW on 6/20/2017.
 */

public enum DriverBuilder {
    INSTANCE;

    private WebDriver driver;
    DriverBuilder() {

        try {
//            System.setProperty("webdriver.chrome.driver","./src/test/resources/drivers/chromedriver.exe");
//            driver = new ChromeDriver();
            ReadPropertyFile reader = new ReadPropertyFile();
            driver = DriverFactory.getDriver(reader.getBrowser());
        } catch (Exception e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public WebDriver getDriver() { return driver; }
}
