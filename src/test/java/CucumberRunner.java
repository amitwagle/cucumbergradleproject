
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by AmitW on 6/20/2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Features/",glue="src/test/java/Steps")
        //format = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json"})

public class CucumberRunner {

}
