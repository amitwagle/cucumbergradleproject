package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

/**
 * Created by AmitW on 7/6/2017.
 */
public class DriverFactory {

//    public enum BrowserType {
//        FIREFOX("firefox"),
//        Chrome("chrome"),
//        IE("internet_explorer"),
//        SAFARI("safari");
//
//        private String value;
//
//        BrowserType (String value){
//            this.value = value;
//        }
//
//        public String getBrowser(){
//            return this.value;
//        }
//    }

    public static WebDriver getDriver(String type){
        WebDriver driver = null;
        switch (type.toUpperCase()){
            case "FIREFOX":
                System.setProperty("webdriver.gecko.driver","./src/test/resources/drivers/geckodriver.exe");
                driver = new FirefoxDriver();
                break;
            case "CHROME":
                System.setProperty("webdriver.chrome.driver","./src/test/resources/drivers/chromedriver.exe");
                driver = new ChromeDriver();
                break;
            case "IE":
                System.setProperty("webdriver.ie.driver","./src/test/resources/drivers/IEDriverServer.exe");
                driver = new InternetExplorerDriver();
                break;
            case "SAFARI":
                driver = new SafariDriver();
                break;
        }
        return driver;
    }

}
