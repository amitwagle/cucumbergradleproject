package Pages;

import static com.jayway.restassured.RestAssured.when;

/**
 * Created by AmitW on 6/28/2017.
 */
public class ServiceTest {

    private String id;
    private String title;
    private String author;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getServiceCode(String url){
        Integer statusCode = when().get(url).getStatusCode();
        return statusCode;
    }

}
