package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by AmitW on 6/18/2017.
 */
public class LogIn_Page {

    WebDriver driver;

    By userName = By.name("userName");
    By passWord = By.name("password");
    By login = By.name("login");

    public LogIn_Page (WebDriver driver){
        this.driver = driver;
    }

    //Set UserName
    public void setUserName(String strUserName){
        driver.findElement(userName).sendKeys(strUserName);
    }

    //Set Password
    public void setPassWord(String strPassword){
        driver.findElement(passWord).sendKeys(strPassword);
    }

    //Click Login
    public void clickLogin(){
        driver.findElement(login).click();
    }

    //Login in the application
    public void LoginToMercuryTours(String strUserName, String strPassword){
        this.setUserName(strUserName);
        this.setPassWord(strPassword);
        this.clickLogin();
    }



}
