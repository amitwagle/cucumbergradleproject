package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by AmitW on 6/25/2017.
 */
public class Register_Page {


    WebDriver driver;

    By register = By.linkText("REGISTER");
    By firstName = By.name("firstName");
    By lastName = By.name("lastName");
    By phone = By.name("phone");
    By email = By.name("userName");

    By address = By.name("address1");
    By city = By.name("city");
    By state = By.name("state");
    By postalCode = By.name("postalCode");
    By country = By.name("country");

    By username = By.name("email");
    By password = By.name("password");
    By confirmPassword = By.name("confirmPassword");
    By submit = By.name("register");

    public Register_Page (WebDriver driver){
        this.driver = driver;
    }

    //Click on Register
    public void clickonRegister(){
       driver.findElement(register).click();
    }

    //Enter Contact information
    public void enterContactInformation(String strFirstName,String strLastName, String strPhone, String strEmail){
        driver.findElement(firstName).sendKeys(strFirstName);
        driver.findElement(lastName).sendKeys(strLastName);
        driver.findElement(phone).sendKeys(strPhone);
        driver.findElement(email).sendKeys(strEmail);

    }

    //Enter mailing information
    public void enterMailingInformation(String strAddress,String strCity, String strState, String strPostalCode,String strCountry){
        driver.findElement(address).sendKeys(strAddress);
        driver.findElement(city).sendKeys(strCity);
        driver.findElement(state).sendKeys(strState);
        driver.findElement(postalCode).sendKeys(strPostalCode);
        Select objCountry = new Select(driver.findElement(country));
        objCountry.selectByVisibleText(strCountry);
    }

    //Enter User information
    public void enterUserInformation(String strUserName,String strPassword, String strConfirmpassword){
        driver.findElement(username).sendKeys(strUserName);
        driver.findElement(password).sendKeys(strPassword);
        driver.findElement(confirmPassword).sendKeys(strConfirmpassword);

    }

    //Click submit
    public void clickSubmit(){
        driver.findElement(submit).click();
    }

}
