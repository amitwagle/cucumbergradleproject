package Pages;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by AmitW on 7/6/2017.
 */
public class ReadPropertyFile {

    protected Properties prop = null;
    protected InputStream input = new FileInputStream("src/config.properties");

    public ReadPropertyFile() throws IOException {
        prop = new Properties();
        prop.load(input);
    }
    public String getUrl(){
        return prop.getProperty("url");
    }
    public String getBrowser(){
        return prop.getProperty("browser");
    }
    public String getUsername(){
        return prop.getProperty("username");
    }
    public String getPassword(){
        return prop.getProperty("password");
    }
    public String getBrowserName(){
        return prop.getProperty("browserName");
    }
    public String getdeviceName(){
        return prop.getProperty("deviceName");
    }
    public String getplatformName(){
        return prop.getProperty("platformName");
    }
    public String getappPackage(){
        return prop.getProperty("appPackage");
    }
    public String getappActivity(){
        return prop.getProperty("appActivity");
    }
}
