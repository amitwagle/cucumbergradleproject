package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by AmitW on 7/11/2017.
 */
public class DesiredCapabilities_Page {

    String browserName;
    String deviceName;
    String platformName;
    String appPackage;
    String appActivity;

    public DesiredCapabilities_Page(String browserName, String deviceName,String platformName,String appPackage,String appActivity) {
        this.browserName = browserName;
        this.deviceName = deviceName;
        this.platformName = platformName;
        this.appPackage = appPackage;
        this.appActivity = appActivity;

    }

    public void setDesiredCapabilities(DesiredCapabilities_Page capValues) throws MalformedURLException {

    // Create an object for Desired Capabilities
    DesiredCapabilities capabilities = new DesiredCapabilities();

    //Name of mobile web browser
    capabilities.setCapability("browserName", capValues.browserName);

    //The kind of mobile device
    capabilities.setCapability("deviceName", capValues.deviceName);

    //Which mobile OS platform to use
    capabilities.setCapability("platformName", capValues.platformName);

    //Java package of the Android app you want to run-
    capabilities.setCapability("appPackage", capValues.appPackage);

    //Activity name for the Android activity you want to launch from your
    capabilities.setCapability("appActivity", capValues.appActivity);

    // Initialize the driver object with the URL to Appium Server and
    // passing the capabilities
    WebDriver driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
    WebDriverWait wait = new WebDriverWait(driver, 5);

    }
}
