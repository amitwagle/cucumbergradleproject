package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by AmitW on 6/20/2017.
 */
public class FlightFinder_Page {

    WebDriver driver;

    By tripType = By.name("tripType");
    By passCount = By.name("passCount");
    By fromPort = By.name("fromPort");
    By fromMonth = By.name("fromMonth");
    By fromDay = By.name("fromDay");
    By toPort = By.name("toPort");
    By toMonth = By.name("toMonth");
    By toDay = By.name("toDay");
    By findFlights = By.name("findFlights");
    By servClass = By.name("servClass");
    By airline = By.name("airline");

    public FlightFinder_Page (WebDriver driver){
        this.driver = driver;
    }

    //Enter Flight Details
    public void enterFlightDetails(String strType,String strPassenger, String strDepFrom, String strDeptMonth,String strDeptDate,String strArrivingIn,String strArrivingMonth,String strArrivingDate){

        //Select Type (One way radio button)
        List<WebElement> oRadioButton = driver.findElements(tripType);
        int iSize = oRadioButton.size();
        System.out.println(iSize);

        for (int i = 0; i < iSize; i++) {
            String sValue = oRadioButton.get(i).getAttribute("value");
            if (sValue.equalsIgnoreCase(strType)) {
                oRadioButton.get(i).click();
                break;
            }
        }

        //Select Passengers
        Select objpassCount = new Select(driver.findElement(passCount));
        objpassCount.selectByVisibleText(strPassenger);

        //Select Departing From
        Select objfromPort = new Select(driver.findElement(fromPort));
        objfromPort.selectByVisibleText(strDepFrom);

        //Select From Month
        Select objfromMonth = new Select(driver.findElement(fromMonth));
        objfromMonth.selectByVisibleText(strDeptMonth);

        //Select From Day
        Select objfromDay = new Select(driver.findElement(fromDay));
        objfromDay.selectByVisibleText(strDeptDate);

        //Select Arriving In
        Select objtoPort = new Select(driver.findElement(toPort));
        objtoPort.selectByVisibleText(strArrivingIn);

        //Select To Month
        Select objtoMonth = new Select(driver.findElement(toMonth));
        objtoMonth.selectByVisibleText(strArrivingMonth);

        //Select To Day
        Select objtoDay = new Select(driver.findElement(toDay));
        objtoDay .selectByVisibleText(strArrivingDate);

    }

    //Enter Preferences
    public void enterPreferences(String strServiceClass, String strAirline){
        //Select Service Class
        List<WebElement> oServiceClass = driver.findElements(servClass);
        int iServiceClassSize = oServiceClass.size();
        System.out.println(iServiceClassSize);

        for (int i = 0; i < iServiceClassSize; i++) {

            String sValue = oServiceClass.get(i).getAttribute("value");
            if (sValue.equalsIgnoreCase(strServiceClass)) {
                oServiceClass.get(i).click();
                break;
            }

        }

        //Select Airline
        Select objairline = new Select(driver.findElement(airline));
        objairline.selectByVisibleText(strAirline);

    }

    //Click Continue
    public void clickContinue(){
        driver.findElement(findFlights).click();
    }


}
