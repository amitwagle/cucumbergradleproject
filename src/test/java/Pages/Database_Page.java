package Pages;

import org.openqa.selenium.WebDriver;

import java.sql.*;

/**
 * Created by AmitW on 6/29/2017.
 */


public class Database_Page {

    Connection conn;

    public  Database_Page(String serverName , String userName ,String passWord, String databaseName ) throws SQLException {

        this.conn = DriverManager.getConnection("jdbc:sqlserver://"+ serverName+ ":1433;databaseName="+
                databaseName+ ";IntegratedSecurity=true");
        System.out.println(serverName);
    }

    public String QueryExecutor(String sqlQuery,String strColumnName) throws SQLException {
        String strSpec="";
        Statement st = conn.createStatement();

        ResultSet rs = st.executeQuery(sqlQuery);
        while( rs.next() ) {
            strSpec = rs.getString(strColumnName);
        }
        return strSpec;
    }


}
